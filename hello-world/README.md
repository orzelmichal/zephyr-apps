# HELLO WORLD APP

This is a simple hello world application to run as a Xen guest.
Tested with Zephyr v3.6.0.

## Expected configuration

The application makes use of Xen HVC console (utilizing console_io hypercall)
which requires using debug version of Xen or passing "guest_loglvl=debug" to
the Xen command line. Application is expected to be run as a dom0less domU.

## Building Xen (Xilinx fork)

```
$ git clone https://github.com/Xilinx/xen.git -b xlnx_rebase_4.18
$ cd xen/xen
$ export XEN_TARGET_ARCH=arm64
$ export CROSS_COMPILE=aarch64-linux-gnu-
$ make -j$(nproc)
```

## Device tree

Example ZCU102 device tree from Petalinux 2023.2 is included as a reference
(system.dts).

## Build Zephyr app for GICv2

```
$ west build -p -b xenvm <path to zephyr-apps/hello-world>
```

## ImageBuilder config file (example for a single dom0less domU on ZCU102)

```
MEMORY_START="0x0"
MEMORY_END="0x80000000"

XEN="xen"
XEN_CMD="console=dtuart dtuart=serial0 bootscrub=0 vwfi=native sched=null"

DEVICE_TREE="system.dtb"

DOMU_KERNEL[0]="zephyr.bin"
DOMU_MEM[0]="16"

NUM_DOMUS="1"

UBOOT_SOURCE="boot.source"
UBOOT_SCRIPT="boot.scr"
```

# Generating U-BOOT boot script

```
$ git clone https://github.com/Xilinx/imagebuilder.git -b xlnx_rel_v2024.1
$ imagebuilder/scripts/uboot-script-gen -c config -d . -t tftp
```

## Results on Xilinx ZCU102

```
Starting kernel ...

 Xen 4.18.0
(XEN) Xen version 4.18.0 (michalo@amd.com) (aarch64-linux-gnu-gcc (GCC) 11.3.1 20220604 [releases/gcc-11 revision 591c0f4b92548e3ae2e8173f4f93984b1c7f62bb]) debug=y Tue May 14 08:17:41 CEST 2024
(XEN) Latest ChangeSet: Mon Apr 15 16:11:17 2024 -0400 git:c4ff3360900b
(XEN) build-id: 7233916933bbb803bbcf17f0eaeb92c417cbe29e
(XEN) Processor: 00000000410fd034: "ARM Limited", variant: 0x0, part 0xd03,rev 0x4
(XEN) 64-bit Execution:
(XEN)   Processor Features: 0000000000002222 0000000000000000
(XEN)     Exception Levels: EL3:64+32 EL2:64+32 EL1:64+32 EL0:64+32
(XEN)     Extensions: FloatingPoint AdvancedSIMD
(XEN)   Debug Features: 0000000010305106 0000000000000000
(XEN)   Auxiliary Features: 0000000000000000 0000000000000000
(XEN)   Memory Model Features: 0000000000001122 0000000000000000
(XEN)   ISA Features:  0000000000011120 0000000000000000
(XEN) 32-bit Execution:
(XEN)   Processor Features: 0000000000000131:0000000000011011
(XEN)     Instruction Sets: AArch32 A32 Thumb Thumb-2 Jazelle
(XEN)     Extensions: GenericTimer Security
(XEN)   Debug Features: 0000000003010066
(XEN)   Auxiliary Features: 0000000000000000
(XEN)   Memory Model Features: 0000000010201105 0000000040000000
(XEN)                          0000000001260000 0000000002102211
(XEN)   ISA Features: 0000000002101110 0000000013112111 0000000021232042
(XEN)                 0000000001112131 0000000000011142 0000000000011121
(XEN) Using SMC Calling Convention v1.2
(XEN) Using PSCI v1.1
(XEN) SMP: Allowing 4 CPUs
(XEN) Generic Timer IRQ: phys=30 hyp=26 virt=27 Freq: 99990 KHz
(XEN) GICv2 initialization:
(XEN)         gic_dist_addr=00000000f9010000
(XEN)         gic_cpu_addr=00000000f9020000
(XEN)         gic_hyp_addr=00000000f9040000
(XEN)         gic_vcpu_addr=00000000f9060000
(XEN)         gic_maintenance_irq=25
(XEN) GICv2: Adjusting CPU interface base to 0x000000f902f000
(XEN) GICv2: 192 lines, 4 cpus, secure (IID 0200143b).
(XEN) Using scheduler: null Scheduler (null)
(XEN) Initializing null scheduler
(XEN) WARNING: This is experimental software in development.
(XEN) Use at your own risk.
(XEN) Allocated console ring of 32 KiB.
(XEN) CPU0: Guest atomics will try 14 times before pausing the domain
(XEN) Bringing up CPU1
(XEN) CPU1: Guest atomics will try 13 times before pausing the domain
(XEN) CPU 1 booted.
(XEN) Bringing up CPU2
(XEN) CPU2: Guest atomics will try 13 times before pausing the domain
(XEN) CPU 2 booted.
(XEN) Bringing up CPU3
(XEN) CPU3: Guest atomics will try 13 times before pausing the domain
(XEN) Brought up 4 CPUs
(XEN) CPU 3 booted.
(XEN) smmu: /axi/smmu@fd800000: probing hardware configuration...
(XEN) smmu: /axi/smmu@fd800000: SMMUv2 with:
(XEN) smmu: /axi/smmu@fd800000:         stage 2 translation
(XEN) smmu: /axi/smmu@fd800000:         stream matching with 48 register groups, mask 0x7fff<2>smmu: /axi/smmu@fd800000:        16 context banks (0 stage-2 only)
(XEN) smmu: /axi/smmu@fd800000:         Stage-2: 48-bit IPA -> 48-bit PA
(XEN) smmu: /axi/smmu@fd800000: registered 0 master devices
(XEN) I/O virtualisation enabled
(XEN)  - Dom0 mode: Relaxed
(XEN) P2M: 40-bit IPA with 40-bit PA and 8-bit VMID
(XEN) P2M: 3 levels with order-1 root, VTCR 0x0000000080023558
(XEN) Scheduling granularity: cpu, 1 CPU per sched-resource
(XEN) Initializing null scheduler
(XEN) WARNING: This is experimental software in development.
(XEN) Use at your own risk.
(XEN) alternatives: Patching with alt table 00000200002dd4c8 -> 00000200002de554
(XEN) Xen dom0less mode detected
(XEN) *** LOADING DOMU cpus=1 memory=0x4000KB ***
(XEN) Loading d1 kernel from boot module @ 0000000000e00000
(XEN) Allocating mappings totalling 16MB for d1:
(XEN) d1 BANK[0] 0x00000040000000-0x00000041000000 (16MB)
(XEN) Grant table range: 0x00000038000000-0x00000039000000
(XEN) Loading zImage from 0000000000e00000 to 0000000040000000-0000000040051004
(XEN) Loading d1 DTB to 0x0000000040e00000-0x0000000040e00455
(XEN) Initial low memory virq threshold set at 0x4000 pages.
(XEN) Std. Loglevel: All
(XEN) Guest Loglevel: All
(XEN) *** Serial input to DOM1 (type 'CTRL-a' three times to switch input)
(XEN) common/sched/null.c:355: 0 <-- d1v0
(XEN) Freed 352kB init memory.
(XEN) d1v0: vGICD: unhandled word write 0x000000ffffffff to ICACTIVER4
(XEN) d1v0: vGICD: unhandled word write 0x000000ffffffff to ICACTIVER8
(XEN) d1v0: vGICD: unhandled word write 0x000000ffffffff to ICACTIVER12
(XEN) d1v0: vGICD: unhandled word write 0x000000ffffffff to ICACTIVER16
(XEN) d1v0: vGICD: unhandled word write 0x000000ffffffff to ICACTIVER20
(XEN) d1v0: vGICD: unhandled word write 0x000000ffffffff to ICACTIVER0
(d1) *** Booting Zephyr OS build v3.6.0 ***
(d1) Hello, World!
```
