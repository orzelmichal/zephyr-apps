/* Simple application to stress caches */

#include <string.h>
#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>

/* When changing, remember to tweak CONFIG_HEAP_MEM_POOL_SIZE */
#define BUF_SIZE MB(2)

static inline void invalidate_local_icache(void)
{
    __asm__ volatile("ic iallu\n");
    __asm__ volatile("dsb nsh\n");
    __asm__ volatile("isb\n");
}

int main(void)
{
    char *src, *dst;

    src = k_malloc(BUF_SIZE);
    if ( !src )
        goto fail;

    dst = k_malloc(BUF_SIZE);
    if ( !dst )
    {
        k_free(src);
        goto fail;
    }

    while ( 1 )
    {
        memcpy(dst, src, BUF_SIZE);
        z_barrier_dsync_fence_full();
        invalidate_local_icache();
    }

fail:
    printk("ERROR: failed to allocate buffers\n");
}
